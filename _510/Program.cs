﻿using static System.Console;


namespace _510
{
    class Program
    {
        static void Main()
        {
            double i, kurs;
            i = 1.0;

            Write("Введите значение курса в $: ");
            kurs = System.Convert.ToDouble(ReadLine());

            while (i <= 20.0)
            {
                WriteLine((int) i + " $ = " + (i * kurs) + " rub");
                i = i + 1.0;
            }

            ReadKey();
        }
    }
}
