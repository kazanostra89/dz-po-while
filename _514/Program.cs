﻿using static System.Console;


namespace _514
{
    class Program
    {
        static void Main()
        {
            int i;
            i = 1;

            while (i < 10)
            {
                WriteLine("9 x " + i + " = " + (9 * i));
                i++;
            }

            ReadKey();
        }
    }
}
