﻿using static System.Console;


namespace _527
{
    class Program
    {
        static void Main()
        {
            //сумма целых чисел от 100 до 500
            /*
            int a, b, summ;
            a = 100;
            b = 500;
            summ = 0;
            while (a <= b)
            {
                summ = summ + a;
                a = a + 1;
            }
            WriteLine("Сумма целых чисел = " + summ);
            */
            //сумма от а до 500
            /*
            int a, summ;
            summ = 0;
            Write("Ведите левый граничный диапазон: ");
            a = System.Convert.ToInt32(ReadLine());

            while (a <= 500)
            {
                summ = summ + a;
                a = a + 1;
            }
            WriteLine("Сумма целых чисел = " + summ);
            */
            //от -10 до b
            /*
            int a, b, summ;
            a = -10;
            summ = 0;
            Write("Ведите правый граничный диапазон: ");
            b = System.Convert.ToInt32(ReadLine());

            while (a <= b)
            {
                summ = summ + a;
                a = a + 1;
            }
            WriteLine("Сумма целых чисел = " + summ);
            */
            //от a до b
            int a, b, summ;
            summ = 0;
            Write("Ведите левый граничный диапазон: ");
            a = System.Convert.ToInt32(ReadLine());
            Write("Ведите правый граничный диапазон: ");
            b = System.Convert.ToInt32(ReadLine());

            while (a <= b)
            {
                summ = summ + a;
                a = a + 1;
            }
            WriteLine("Сумма целых чисел = " + summ);

            ReadLine();
        }
    }
}
