﻿using static System.Console;


namespace _53
{
    class Program
    {
        static void Main()
        {
            /*
            //все целые числа от 20 до 35
            int i;
            i = 20;
            while (i <= 35)
            {
                WriteLine(i);
                i++;
            }
            */
            // квадраты всех целых чисел от 10 до b
            /*
            int i, b;
            i = 10;
            Write("Введите правое пограничное целое цисло b>=10: ");
            b = System.Convert.ToInt32(ReadLine());
            if (b >= 10)
            {
                while (i <= b)
                {
                    WriteLine("Квадрат числа " + i + " = " + System.Convert.ToInt32(System.Math.Pow(i, 2.0)));
                    i++;
                }
            }
            else
            {
                WriteLine("Вы ввели число не соответствующее условию");
            }
            */
            //Третьи степени всех целых чисел от а до 50, а <= 50
            /*
            int a;
            Write("Введите левое пограничное целое цисло a<=50: ");
            a = System.Convert.ToInt32(ReadLine());
            if (a <= 50)
            {
                while (a <= 50)
                {
                    WriteLine("Куб числа " + a + " = " + System.Convert.ToInt32(System.Math.Pow(a, 3.0)));
                    a++;
                }
            }
            else
            {
                WriteLine("Вы ввели число не соответствующее условию");
            }
            */
            //все целые числа от a до b, значения вводятся с клавиатуры (b>=a)
            int a, b;
            WriteLine("Введите числа a и b, соответствующие условию b>=a");
            Write("Введите левое пограничное целое цисло a: ");
            a = System.Convert.ToInt32(ReadLine());
            Write("Введите правое пограничное целое цисло b: ");
            b = System.Convert.ToInt32(ReadLine());
            if (b >= a)
            {
                while (a <= b)
                {
                    WriteLine(a);
                    a++;
                }
            }
            else
            {
                WriteLine("Вы ввели неверный диапазон");
            }


            ReadKey();
        }
    }
}
